import axios from ".";

/**
 * Fetch products from a REST API
 * @returns { Promise<{ products: [], error: null | string }>} response
 */
export const fetchProducts = async () => {
  
  const productsURL = "http://localhost:8000/products";
  
  try {
    const { data } = await axios.get(productsURL);
    return Promise.resolve({
      products: data,
      error: null,
    });
  } 
  catch (e) {
    return Promise.reject({
      products: [],
      error: e.message,
    });
  }
};
