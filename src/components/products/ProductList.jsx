import useProducts from "../../hooks/useProducts";

function ProductList() {
  const { products, error } = useProducts();

  if (error) {
    return <p>{error}</p>;
  }

  if (!products) {
    return <p>Loading products...</p>;
  }

  return (
    <div>
      <ul>
        {products.map((p) => (
          <li key={p.id}>
            <span>{p.name}</span> - <span>{p.price}</span>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default ProductList;
